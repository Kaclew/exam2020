# Exam 2020 #

# IMPORTANT! #
Fix 24/05
Due to some black magic, my CMake integration code was reverted back so the code tried to access root which you did not have. This has been fixed. 

## Setup instruction ##
To make the program run, please run Cmake on the root folder to get the whole program up and running. (Same way we did it during the labs) 
This project should not need anything else other than what is already included in the repository. In case Cmake does not work, you can try to manually 
include all the necessary files. All files in src, include and all the cpp/h files in folder external are required this way.

## Controls ##
You control the camera with arrow buttons **(NOT WASD)**. You can move camera around with the mouse. You can zoom in with **E** and zoom out with **Q**.
By pressing **T** you will go into the bird view. Pressing **W / S** will let you move up and down, this is mostly for debug but I considered it useful enough to keep it.

## Implemented Features ##
- Fully customizable map
- Camera movement, perspective switching and zooming
- Render distance
- Dynamic day/night cycle
- Loading objects
- The animals move around

## What I wish was improved ##
- The animals get new destination every frame, so that makes them move around in the same spot a lot
- Textures on the animals are bad

## Self-made stuff ##
- Due to my pc not being the greatest, i had to even further decrease the amount of polygons on some of the objects. Deer and plane got self-made objects to make it easier for me to debug
- Some extra textures were implemented into my program

## Comments ##
- Due to me being sometimes slow, there aren't as many commits as I wish there were mostly because I would often be stuck on one problem for prolonged period of time.
- If there are any problems with the repository or the program, or any questions, feel free to write an issue. 