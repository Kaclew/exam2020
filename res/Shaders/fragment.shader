#version 430 core

/** Inputs */
layout(location = 0) in vec2 vsTex;
layout(location = 1) in vec3 skyColor;

//Uniforms
layout(location = 0) uniform sampler2D uTextureA;

/** Outputs */
layout(location = 0) out vec4 outColor;


vec3 lighting() {
    vec3 ambient = skyColor; 

    return ambient;
}


void main()
{
    vec4 tex = texture(uTextureA, vsTex);
    vec3 lighting = lighting();
    outColor = vec4(tex.rgb * lighting, tex.a);
    if (outColor.a < 0.1) {
        discard;
    }
}
