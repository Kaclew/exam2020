#pragma once

#ifndef __MANAGER_H
#define __MANAGER_H

#include <vector>

#include <glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "loader.h"
#include "entity.h"

/*Used to create an abstract map of level
P = player, g = ground, h = hill, w = water, f = fish, d = doe, p = plane, o = arbitrary forest 1, k = arbitrary forest 2, l = arbitrary forest 3*/
typedef enum { P, g, h, w, f, d, p, o, k, l } tile;

//Size of the levels
int const SIZE_X = 28;
int const SIZE_Z = 36;

//-----------------------
//Map based tile functions
//-----------------------

//Loads the map from a file
void loadMap(const char* source);

//Find what kind of enum specified number has
tile findTile(GLfloat x, GLfloat z);

//Spawns all objects into the map
glm::vec3 spawn(Loader* loaderSingleton, std::vector<Entity>* walls, std::vector<Entity>* ghosts, std::vector<Entity>* pizzas, std::vector<Entity>* tiles, std::vector<Entity>* fish, std::vector<Entity>* tree, std::vector<Entity>* plane);

//-----------------------
//Logic based tile functions
//-----------------------

//Decides in which direction the entity is supposed to move in 
//Type clarification: 0 - fish / plane | 1 - deer 
glm::vec3 moveEntity(Entity entity, int type);


#endif
