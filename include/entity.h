#pragma once

#ifndef __ENTITY_H
#define __ENTITY_H

#include <iostream>
#include <vector>

#include <glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "vertex.h"
#include "mesh.h"

class Entity {
private:
	//EVERY ENTITY

	//Contains all meshes for an entity.
	std::vector<Mesh> meshes;
	//Contains all vertex objects as a big vector
	std::vector<Vertex> uniqueVertices;

	//Each entity has a VAO and a VBO, whereas EBO is controlled by individual meshes
	GLuint VAO;
	GLuint VBO;

	glm::vec3 position = glm::vec3(0.f);

	//Used by destructor
	bool trueDestroy = false;

public:
	Entity() = default;
	~Entity();

	//-----------------------
	//OpenGL specific functions
	//-----------------------

	void initialise();

	//Adds unique vertices to the entity. Used only in LOADER
	void addUniqueVertices(std::vector<Vertex> newUniqueVertices);

	//Adds mesh to the entity. Used only in LOADER
	void addMesh(Mesh mesh);

	//Resizes all meshes for an entity. Used only in MANAGER
	void resize(glm::vec3 newScale);

	//Rotates all meshes for an entity. Used only in MANAGER
	void rotate(float radians, char axis);

	//Sets rotation to 0 for all meshes in an entity. Not used
	void clearRotation();

	//Moves an entity and all its meshes.
	void move(float distance, char axis);

	//Sets position for all meshes for an entity
	void setPosition(glm::vec3 newPosition);

	//Gets position of the entity
	glm::vec3 getPosition();

	//Draws all meshes for an entity
	void draw(GLuint shader);

	//-----------------------
	//Game specific functions
	//-----------------------

	void move3D(double delta, glm::vec3(direction));
};

#endif
