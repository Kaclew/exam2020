#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <random>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>

#include <glad.h>

#include "manager.h"
#include "loader.h"
#include "entity.h"

GLfloat spawnPosition[2];

//The abstract map - id est the level - itself
tile mapAbstract[SIZE_X][SIZE_Z] = {};

void loadMap(const char* source)
{
	std::ifstream text{ source };
	char c;
	int i = 0, j = 0;

	//Gets single character and checks what type it is
	while (text.get(c)) {
		if (c == 'w') {
			mapAbstract[i][j] = w;
			i++;
		}
		if (c == 'g') {
			mapAbstract[i][j] = g;
			i++;
		}
		if (c == 'P') {
			mapAbstract[i][j] = P;
			i++;
		}
		if (c == 'h') {
			mapAbstract[i][j] = h;
			i++;
		}
		if (c == 'd') {
			mapAbstract[i][j] = d;
			i++;
		}
		if (c == 'f') {
			mapAbstract[i][j] = f;
			i++;
		}
		if (c == 'o') {
			mapAbstract[i][j] = o;
			i++;
		}
		if (c == 'k') {
			mapAbstract[i][j] = k;
			i++;
		}
		if (c == 'l') {
			mapAbstract[i][j] = l;
			i++;
		}
		if (c == 'p') {
			mapAbstract[i][j] = p;
			i++;
		}

		//The while loop is the same as:
		//for (int j = 0; j < 36; j++)
			// for (int i = 0; i < 28; i++)
		if (i > 27) {
			j++;
			i = 0;
		}
	}

	text.close();
}

std::vector<GLfloat> findCoordinates(unsigned int x, unsigned int z) {
	//Creates two floats in a vector.
	std::vector<GLfloat> coordinates;
	coordinates.push_back(0.0f);
	coordinates.push_back(0.0f);

	//Add the position on tile + half to get the middle of that tile
	coordinates[0] = float(x + 0.5f);
	coordinates[1] = float(z + 0.5f);

	return coordinates;
}

tile findTile(GLfloat x, GLfloat z) {
	return mapAbstract[int(floor(x))][int(floor(z))];
}

glm::vec3 spawn(Loader* loaderSingleton, std::vector<Entity>* ground, std::vector<Entity>* water, std::vector<Entity>* hill, std::vector<Entity>* doe, std::vector<Entity>* fish, std::vector<Entity>* tree, std::vector<Entity>* plane) {
	glm::vec3 spawnPosition = glm::vec3(0.f);

	Texture tempGroundTexture;
	Texture tempWaterTexture;
	Texture tempHillTexture;
	Texture tempTileTexture;
	Texture tempDoeTexture;
	Texture tempFishTexture;
	Texture tempTreeTexture;
	Texture tempPlaneTexture;

	tempGroundTexture.load("./res/Textures/ground.jpg");
	tempWaterTexture.load("./res/Textures/wata.png");
	tempHillTexture.load("./res/Textures/monta.png");
	tempDoeTexture.load("./res/Textures/doe.png");
	tempFishTexture.load("./res/Textures/fish.png");
	tempTreeTexture.load("./res/Textures/tree.png");
	tempPlaneTexture.load("./res/Textures/plane.jpg");

	for (int z = 0; z < SIZE_Z; z++) {
		for (int x = 0; x < SIZE_X; x++) {
			std::vector<GLfloat> coordinates = findCoordinates(x, z);

			//Checks if tile is ground or player and spawns ground
			if (mapAbstract[x][z] == g || mapAbstract[x][z] == P || mapAbstract[x][z] == o || mapAbstract[x][z] == k || mapAbstract[x][z] == l || mapAbstract[x][z] == d || mapAbstract[x][z] == p) {
				//Very big if statement that checks whether the ground is not out of bounds and if it's surrounded with water.
				//If it is, create a cube, otherwise generate a sprite for improved performance.
				if (x - 1 > -1 && x + 1 < 26 && z - 1 > -1 && z + 1 < 35 && mapAbstract[x - 1][z] == w || mapAbstract[x + 1][z] == w || mapAbstract[x][z - 1] == w || mapAbstract[x][z + 1] == w) {
					Entity tempGround = loaderSingleton->load("./res/Objects/cube.obj", tempGroundTexture);
					tempGround.resize(glm::vec3(0.5f));
					tempGround.initialise();
					tempGround.move(-1.f, 'y');
					tempGround.move(coordinates[0], 'x');
					tempGround.move(coordinates[1], 'z');
					ground->push_back(tempGround);
				}
				else {
					Entity tempGround = loaderSingleton->loadSprite(tempGroundTexture);
					tempGround.initialise();
					tempGround.move(coordinates[0], 'x');
					tempGround.move(coordinates[1], 'z');
					tempGround.move(-0.5f, 'y');
					tempGround.move(-0.5f, 'z');
					tempGround.move(-0.5f, 'x');
					tempGround.rotate(90.f, 'x');
					ground->push_back(tempGround);
				}
			}

			//Loads water
			if (mapAbstract[x][z] == w || mapAbstract[x][z] == f) {
				Entity tempWater = loaderSingleton->loadSprite(tempWaterTexture);
				tempWater.initialise();
				tempWater.move(coordinates[0], 'x');
				tempWater.move(coordinates[1], 'z');
				tempWater.move(-1.5f, 'y');
				tempWater.move(-0.5f, 'z');
				tempWater.move(-0.5f, 'x');
				tempWater.rotate(90.f, 'x');
				water->push_back(tempWater);
			}

			//Loads hill
			if (mapAbstract[x][z] == h) {
				Entity tempHill = loaderSingleton->load("./res/Objects/cube.obj", tempHillTexture);
				tempHill.resize(glm::vec3(0.5f));
				tempHill.initialise();
				tempHill.move(coordinates[0], 'x');
				tempHill.move(coordinates[1], 'z');
				hill->push_back(tempHill);
			}

			//Loads does
			if (mapAbstract[x][z] == d) {
				Entity tempDoe = loaderSingleton->load("./res/Objects/deer.obj", tempDoeTexture);
				tempDoe.resize(glm::vec3(0.2f));
				tempDoe.initialise();
				tempDoe.move(-0.5f, 'y');
				tempDoe.move(coordinates[0], 'x');
				tempDoe.move(coordinates[1], 'z');
				doe->push_back(tempDoe);
			}

			//Loads fish
			if (mapAbstract[x][z] == f) {
				Entity tempFish = loaderSingleton->load("./res/Objects/fish.obj", tempFishTexture);
				tempFish.resize(glm::vec3(0.1f));
				tempFish.initialise();
				tempFish.move(-1.4f, 'y');
				tempFish.move(coordinates[0], 'x');
				tempFish.move(coordinates[1], 'z');
				fish->push_back(tempFish);
			}

			//Loads plane
			if (mapAbstract[x][z] == p) {
				Entity tempPlane = loaderSingleton->load("./res/Objects/plane.obj", tempPlaneTexture);
				tempPlane.resize(glm::vec3(0.1f));
				tempPlane.initialise();
				tempPlane.move(+2.0f, 'y');
				tempPlane.move(coordinates[0], 'x');
				tempPlane.move(coordinates[1], 'z');
				plane->push_back(tempPlane);
			}

			//Spawns first tile of arbitrary trees.
			if (mapAbstract[x][z] == o) {
				//There are 3 trees in this tile
				for (int k = 0; k < 3; k++) {
					std::random_device seed{};
					std::mt19937 generator(seed());
					std::uniform_real_distribution<float> rng(-1.5, 1.5);
					float randomTreePositionX = rng(generator); //Generate random position for trees from -1.5 to 1.5
					float randomTreePositionY = rng(generator);

					Entity tempTree = loaderSingleton->load("./res/Objects/tree.obj", tempTreeTexture);
					tempTree.resize(glm::vec3(0.1f));
					tempTree.initialise();
					tempTree.move(-0.5f, 'y');
					tempTree.move(coordinates[0] + randomTreePositionX, 'x');
					tempTree.move(coordinates[1] + randomTreePositionY, 'z');
					tree->push_back(tempTree);
				}
			}

			if (mapAbstract[x][z] == k) {
				//There are 5 trees in this tile
				for (int k = 0; k < 5; k++) {
					std::random_device seed{};
					std::mt19937 generator(seed());
					std::uniform_real_distribution<float> rng(-1.5, 1.5);
					float randomTreePositionX = rng(generator);	//Generate random position for trees from -1.5 to 1.5
					float randomTreePositionY = rng(generator);

					Entity tempTree = loaderSingleton->load("./res/Objects/tree.obj", tempTreeTexture);
					tempTree.resize(glm::vec3(0.1f));
					tempTree.initialise();
					tempTree.move(-0.5f, 'y');
					tempTree.move(coordinates[0] + randomTreePositionX, 'x');
					tempTree.move(coordinates[1] + randomTreePositionY, 'z');
					tree->push_back(tempTree);
				}
			}

			//Setting spawnpoint
			if (mapAbstract[x][z] == P) {
				spawnPosition.x = x + 0.5f;
				spawnPosition.y = 0.f;
				spawnPosition.z = z + 0.5f;
			}
		}
	}

	return spawnPosition;
}

glm::vec3 moveEntity(Entity entity, int type) {
	//Random generator device for AI
	std::random_device seed{};
	std::mt19937 generator(seed());
	std::uniform_real_distribution<float> floatRNG(0, 1.5);
	std::uniform_int_distribution<int> rng(0, 3);
	int directions[4];		//Checks which directions are valid

	int chosenDirection; int vacantDirections = 0;

	glm::vec3 entityPosition = entity.getPosition();

	//Checking which tiles are valid to walk through
	//Checks for fishes
	if (type == 0) {
		//North
		if (findTile(entityPosition.x, entityPosition.z + 1) != g) {
			directions[0] = true;
			vacantDirections++;
		}
		//East
		if (findTile(entityPosition.x + 1, entityPosition.z) != g) {
			directions[1] = true;
			vacantDirections++;
		}
		//South
		if (findTile(entityPosition.x, entityPosition.z - 1) != g) {
			directions[2] = true;
			vacantDirections++;
		}
		//West
		if (findTile(entityPosition.x - 1, entityPosition.z) != g) {
			directions[3] = true;
			vacantDirections++;
		}
	}

	//Check for doe
	if (type == 1) {
		//North
		if (findTile(entityPosition.x, entityPosition.z + 1) != w || findTile(entityPosition.x, entityPosition.z + 1) != h) {
			directions[0] = true;
			vacantDirections++;
		}
		//East
		if (findTile(entityPosition.x, entityPosition.z + 1) != w || findTile(entityPosition.x, entityPosition.z + 1) != h) {
			directions[1] = true;
			vacantDirections++;
		}
		//South
		if (findTile(entityPosition.x, entityPosition.z + 1) != w || findTile(entityPosition.x, entityPosition.z + 1) != h) {
			directions[2] = true;
			vacantDirections++;
		}
		//West
		if (findTile(entityPosition.x, entityPosition.z + 1) != w || findTile(entityPosition.x, entityPosition.z + 1) != h) {
			directions[3] = true;
			vacantDirections++;
		}
	}

	//Direction in which it will go
	glm::vec3 desiredDestination = glm::vec3(0);

	//Randomly decides direction in which the entity can move, unless the direction is invalid
	for (unsigned int i = 0; i < 2; i++) {
		do {
			chosenDirection = rng(generator);
		} while (!(directions[chosenDirection]));

		float chosenLength = floatRNG(generator);
		//North
		switch (chosenDirection) {
		case 0: {
			desiredDestination.z += chosenLength;
			break;
		}
		//East
		case 1: {
			desiredDestination.x += chosenLength;
			break;
		}
		//South
		case 2: {
			desiredDestination.z -= chosenLength;
			break;
		}
		//West
		case 3: {
			desiredDestination.x -= chosenLength;
			break;
		}
		}
	}
	return desiredDestination;
}