#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <math.h>

#include <glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "vertex.h"
#include "mesh.h"
#include "entity.h"
#include "loader.h"
#include "camera.h"
#include "manager.h"
#include "texture.h"
#include "window.h"

int main() {
	auto window = initializeWindow();
	glClearColor(1.f, 0.75f, 0.79f, 1);

	//Initialising some singletons
	Loader loaderSingleton;
	Camera cameraMan;

	//Compiling the shader
	auto shader = makeShader("./res/Shaders/fragment.shader", "./res/Shaders/vertex.shader");
	glUseProgram(shader);

	//Creating storages for entities
	std::vector<Entity> ground;
	std::vector<Entity> hill;
	std::vector<Entity> water;
	std::vector<Entity> doe;
	std::vector<Entity> fish;
	std::vector<Entity> tree;
	std::vector<Entity> plane;

	//Some gameflow variables
	glm::vec3 spawnPosition;
	double xpos, ypos;
	bool firstFrame = true;				//There is very few fps at the start, this is to prevent it.
	double before = 0;					
	int renderDistance = 7;				//Used to determine renderable distance. (This is primarily for me and my pc)
	int cameraMode = 0;					//Flight view or first person view
	double time = 0;					//game time, decides the current in-game time
	const int TIMEPERMODE = 12;			//Decides how "long" a day lasts. 
	int timeMode = 0;					//Decides what type of time is it. 0 = Day | 1 = Evening  | 2 = Night | 3 = Morning | 4 = About to be day
	glm::vec3 timeColor = glm::vec3(1);	//Is sent to the shader

	//Loading the map
	std::string levelText = "./res/map1.txt";
	loadMap(levelText.c_str());

	//Spawns all entities 
	spawnPosition = spawn(&loaderSingleton, &ground, &water, &hill, &doe, &fish, &tree, &plane);
	cameraMan.setCameraPosition(spawnPosition);

	//OpenGL window loop
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(shader);

		//Delta time stuff
		double now = glfwGetTime();
		double delta = now - before;
		if (firstFrame) {
			delta = 0;
			firstFrame = false;
		}
		before = now;

		//----------------------
		//Ambient light modulator
		//---------------------
		time += delta;

		if (time > TIMEPERMODE) {
			time = 0;
			timeMode++;
			if (timeMode == 4) {
				timeMode = 0;
			}
		}

		if (timeMode == 0) {
			if (time > TIMEPERMODE / 2) {
				timeColor.x -= 0.0045;
				if (timeColor.x < 0.6) {
					timeColor.x = 0.6;
				}
				timeColor.y -= 0.006525;
				if (timeColor.y < 0.6) {
					timeColor.y = 0.6;
				}
				timeColor.z -= 0.01;
				if (timeColor.z < 0.2) {
					timeColor.z = 0.2;
				}
			}
		}

		if (timeMode == 1) {
			if (time > TIMEPERMODE / 2) {
				timeColor.x -= 0.003;
				if (timeColor.x < 0.2) {
					timeColor.x = 0.2;
				}

				timeColor.y -= 0.003;
				if (timeColor.y < 0.2) {
					timeColor.y = 0.2;
				}

				timeColor.z += 0.006;
				if (timeColor.z > 0.7) {
					timeColor.z = 0.7;
				}

			}
		}

		if (timeMode == 2) {
			if (time > TIMEPERMODE / 2) {
				timeColor.x += 0.003;
				if (timeColor.x > 0.6) {
					timeColor.x = 0.6;
				}

				timeColor.y += 0.003;
				if (timeColor.y > 0.6) {
					timeColor.y = 0.6;
				}

				timeColor.z -= 0.006;
				if (timeColor.z < 0.2) {
					timeColor.z = 0.2;
				}


			}
		}

		if (timeMode == 3) {
			if (time > TIMEPERMODE / 2) {
				timeColor.x += 0.0045;
				timeColor.y += 0.00675;
				timeColor.z += 0.01;
			}
			if (time > TIMEPERMODE - 0.5) {
				timeColor = glm::vec3(1);
			}
		}

		glUniform3fv(glGetUniformLocation(shader, "timeColor"), 1, glm::value_ptr(timeColor));

		//-----------------------
		//		  INPUT
		//-----------------------
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			cameraMan.moveCamera(1, delta);
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			cameraMan.moveCamera(3, delta);
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			cameraMan.moveCamera(0, delta);
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			cameraMan.moveCamera(2, delta);
		}

		//Moving up
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			glm::vec3 position = cameraMan.getCameraPosition();
			position.y += 0.3f;
			cameraMan.setCameraPosition(position);
		}

		//Moving down
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			glm::vec3 position = cameraMan.getCameraPosition();
			position.y -= 0.3f;
			cameraMan.setCameraPosition(position);
		}

		//Zooming out
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
			cameraMan.zoom(-1, delta);
		}

		//Zooming in
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			cameraMan.zoom(1, delta);
		}


		//Top-down view
		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
			//Transitioning from first person to third person
			if (cameraMode == 0) {
				cameraMode = 1;
				glm::vec3 cameraPos = cameraMan.getCameraPosition();
				cameraPos.y = 10.f;
				cameraMan.setCameraPosition(cameraPos);
				cameraMan.setCameraFace(glm::vec3(0.f, -1.f, -0.7f));
				renderDistance = 20;
			}
			//Transitioning from third person to first person
			else if (cameraMode == 1) {
				(cameraMode = 0);
				glm::vec3 cameraPos = cameraMan.getCameraPosition();
				cameraPos.y = 0.f;
				cameraMan.setCameraPosition(cameraPos);
				cameraMan.setCameraFace(glm::vec3(0, 1, 0));
				renderDistance = 10;
			}
		}

		//Mouse controls
		if (cameraMode == 0) {
			glfwGetCursorPos(window, &xpos, &ypos);
			cameraMan.mouseCallback(xpos, ypos);
		}
		cameraMan.calculateView(shader);

		glfwPollEvents();


		//  --------------------
		//		RENDERING
		//  --------------------
		glm::vec3 cameraPosition = cameraMan.getCameraPosition();

		//Renders ground
		for (unsigned int i = 0; i < ground.size(); i++) {
			glm::vec3 difference = cameraPosition - ground[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				ground[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
		}

		//Renders water
		for (unsigned int i = 0; i < water.size(); i++) {
			glm::vec3 difference = cameraPosition - water[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				water[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
		}

		//Renders hill
		for (unsigned int i = 0; i < hill.size(); i++) {
			glm::vec3 difference = cameraPosition - hill[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				hill[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
		}

		//Renders doe
		for (unsigned int i = 0; i < doe.size(); i++) {
			glm::vec3 difference = cameraPosition - doe[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				doe[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
			glm::vec3 destination = moveEntity(doe[i], 0);
			doe[i].move3D(delta, destination);

		}

		//Renders trees
		for (unsigned int i = 0; i < tree.size(); i++) {
			glm::vec3 difference = cameraPosition - tree[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				tree[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
		}

		//Renders fish
		for (unsigned int i = 0; i < fish.size(); i++) {
			glm::vec3 difference = cameraPosition - fish[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				fish[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);
			
			glm::vec3 destination = moveEntity(fish[i], 0);
			fish[i].move3D(delta, destination);

		}


		//Renders plane
		for (unsigned int i = 0; i < plane.size(); i++) {
			glm::vec3 difference = cameraPosition - plane[i].getPosition();
			glDisable(GL_CULL_FACE);

			if (length(difference) < renderDistance) {
				plane[i].draw(shader);
			}
			glEnable(GL_CULL_FACE);

			glm::vec3 destination = moveEntity(plane[i], 0);
			plane[i].move3D(delta, destination);

		}


		glfwSwapBuffers(window);
	}

	glUseProgram(0u);
	glDeleteProgram(shader);
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}